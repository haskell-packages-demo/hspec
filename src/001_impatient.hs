import safe "base"
  Control.Exception
    ( evaluate )
import safe "hspec"
  Test.Hspec ()
import safe "QuickCheck"
  Test.QuickCheck ()

main :: IO ()
main = hspec $ do
  describe "Prelude.head" $ do
    it "returns the first element of a list" $ do
      head [23 ..] `shouldBe` (23 :: Int)

    it "returns the first element of an *arbitrary* list" $
      property $ \x xs -> head (x:xs) == (x :: Int)

    it "throws an exception if used with an empty list" $ do
      evaluate (head []) `shouldThrow` anyException

{-
Based on:
https://hspec.github.io
Version: 2024-05 2.11.9
-}
